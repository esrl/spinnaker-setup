from debian:9

# Install packages
WORKDIR /root/
RUN apt update
RUN apt upgrade -y
RUN apt install -y git
RUN apt install -y vim
RUN apt install -y perl
RUN apt install -y perl-tk
RUN apt install -y libterm-readline-gnu-perl
RUN apt install -y wget
RUN apt install -y dtrx
RUN apt install -y make
RUN apt install -y cmake
RUN apt install -y gcc-arm-none-eabi
RUN apt install -y python-pip 
RUN apt install -y python-tk
RUN apt install -y zsh
RUN apt install -y locate
RUN apt install -y tree
RUN apt upgrade -y
RUN apt update
RUN apt clean

# Install python packages
WORKDIR /root/
RUN pip install sPyNNaker8
RUN pip install networkx
RUN pip install matplotlib
RUN pip install pygame
RUN python -m spynnaker8.setup-pynn

# Clone spinnaker tools tools & default config
WORKDIR /root/
RUN wget https://gitlab.com/esrl/spinnaker-setup/raw/master/.spynnaker.cfg
RUN git clone https://github.com/SpiNNakerManchester/SupportScripts.git	SupportScripts

# Install high-level tools
WORKDIR /root/SupportScripts/
RUN yes | ./install.sh PyNN8

# Install low-level tools
WORKDIR /root/SupportScripts/spinnaker_tools/
RUN export SPINN_DIRS=/root/SupportScripts/spinnaker_tools/ && \
    export PATH=$PATH:/root/SupportScripts/spinnaker_tools/tools/ && \
    export PERL5LIB=$PERL5LIB:/root/SupportScripts/spinnaker_tools/tools && \
    /bin/bash -c 'source setup' && \
     make

#WORKDIR /root/SupportScripts/
#RUN /bin/bash -c 'export PYTHONPATH=$PYTHONPATH:/root/SupportScripts/SpiNNUtils && ./automatic_make.sh'

# Automaticly set enviroment variables
WORKDIR /root/
RUN echo "source /root/SupportScripts/spinnaker_tools/setup" >> .bashrc
RUN echo "export NEURAL_MODELLING_DIRS=/root/SupportScripts/sPyNNaker/neural_modelling" >> .bashrc

# Install Brian2
WORKDIR /root/
RUN pip install brian2

# Install NEST
WORKDIR /root/SupportScripts/
RUN wget https://github.com/nest/nest-simulator/archive/v2.16.0.tar.gz
RUN dtrx v2.16.0.tar.gz
WORKDIR /root/SupportScripts/v2.16.0/nest-simulator-2.16.0/
RUN mkdir build
WORKDIR /root/SupportScripts/v2.16.0/nest-simulator-2.16.0/build/
RUN cmake -DCMAKE_INSTALL_PREFIX:PATH=/root/SupportScripts/. ..
RUN make -j9
RUN make install
WORKDIR /root/
RUN echo "source /root/SupportScripts/v2.16.0/nest-simulator-2.16.0/build/extras/nest_vars.sh" >> .bashrc

# # RUN mkdir neuron
#WORKDIR /root/neuron/
#RUN wget https://neuron.yale.edu/ftp/neuron/versions/v7.6/7.6.5/nrn-7.6.5.tar.gz
#RUN wget https://neuron.yale.edu/ftp/neuron/versions/v7.6/iv-19.tar.gz
#RUN dtrx nrn-7.6.5.tar.gz
#RUN dtrx iv-19.tar.gz
#RUN mv nrn-7.6.5/ nrn/
#RUN mv iv-19/ iv/
#RUN rm *.tar.gz
#WORKDIR /root/neuron/iv/
#RUN ./configure --prefix=`pwd`
#RUN make
#RUN make install
#WORKDIR /root/neuron/nrn/
#RUN ./configure --prefix=`pwd` --with-iv=$HOME/neuron/iv
#RUN make
#RUN make install

# Install Oh my zsh
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh
RUN cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
RUN echo "export PROMPT='%n \${ret_status}\${reset_color}%/ \$ '" >> .zshrc

WORKDIR /root/
