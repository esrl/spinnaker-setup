library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity SpinLinkReceiver is
    port(
        clk : in std_logic := '0';
        reset : in std_logic := '0';
        error : out std_logic := '0';
        
        lin : in std_logic_vector(6 downto 0) := (others => '0');
        linack : out std_logic := '0';

        header : out std_logic_vector(7 downto 0) := (others => '0');
        key : out std_logic_vector(31 downto 0) := (others => '0');
        data : out std_logic_vector(31 downto 0) := (others => '0');
        data_packet : out std_logic := '0';
        packet_req : out std_logic := '0'
    );
end SpinLinkReceiver;

architecture behavioral of SpinLinkReceiver is

    signal packet_req_temp : std_logic := '0';

    signal ack : std_logic := '0';
    signal last : std_logic_vector(6 downto 0)  := (others => '0');
    signal diff : std_logic_vector(6 downto 0)  := (others => '0');

    signal packet : std_logic_vector(71 downto 0)  := (others => '0');

    signal i : unsigned(4 downto 0) := (others => '0');

    signal idle_count : unsigned(31 downto 0) := (others => '0');

    signal symbol : std_logic_vector(4 downto 0) := (others => '0');

begin

    header <= packet(7 downto 0);
    key <= packet(39 downto 8);
    data <= packet(71 downto 40);

    diff <= lin xor last;

    packet_req <= packet_req_temp;
    linack <= ack;

    symbol <=
        "00000" when diff = "0010001" else
        "00001" when diff = "0010010" else
        "00010" when diff = "0010100" else
        "00011" when diff = "0011000" else
        "00100" when diff = "0100001" else
        "00101" when diff = "0100010" else
        "00110" when diff = "0100100" else
        "00111" when diff = "0101000" else
        "01000" when diff = "1000001" else
        "01001" when diff = "1000010" else
        "01010" when diff = "1000100" else
        "01011" when diff = "1001000" else
        "01100" when diff = "0000011" else
        "01101" when diff = "0000110" else
        "01110" when diff = "0001100" else
        "01111" when diff = "0001001" else
        "10000" when diff = "1100000" else
        "11111";

    process (clk) begin
        if (rising_edge(clk)) then
            if (reset = '1') then
                ack <= not ack;
                last <= lin;
                idle_count <= (others => '0');
            elsif (symbol < "10000") then
                i <= i + 1;
                packet(3 + to_integer(i) * 4 downto to_integer(i) * 4) <= symbol(3 downto 0);
                last <= lin;
                ack <= not ack;
                idle_count <= (others => '0');
            elsif (symbol = "10000") then
                packet_req_temp <= not packet_req_temp;
                ack <= not ack;
                idle_count <= (others => '0');
                last <= lin;
                i <= (others => '0');
            if (i = 10) then
                data_packet <= '0';
                error <= '0';
            elsif (i = 16) then
                data_packet <= '1';
                error <= '0';
            else
                error <= '1';
            end if;
            else
                if (idle_count > 1024) then
                    ack <= not ack;
                    idle_count <= (others => '0');
                else
                    idle_count <= idle_count + 1;
                end if;
            end if;
        end if;
    end process;

end behavioral;
