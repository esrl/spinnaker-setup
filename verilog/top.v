`timescale 1ns / 1ps

module
    top(
        input wire clk,
        input wire[3:0] btn,
        input wire[3:0] sw,
        output reg[3:0] led = 0,
        
        // RX
        input wire[6:0] rx_tos,
        output wire rx_tos_ack,
        
        // TX
        output wire[6:0] tx_tos,
        input wire tx_tos_ack
    );
    
    // input button debounce
    reg[3:0] dbtn = 0;
    reg[31:0] debounce1 = 0;
    reg[31:0] debounce2 = 0;
    reg[31:0] debounce3 = 0;
    reg[31:0] debounce4 = 0;
    
    always @(posedge clk) begin
        debounce1 = {debounce1[30:0], btn[0]};
        debounce2 = {debounce2[30:0], btn[1]};
        debounce3 = {debounce3[30:0], btn[2]};
        debounce4 = {debounce4[30:0], btn[3]};
    end

    always@(*) begin
        if (debounce1 == 32'h00000000) dbtn[0] = 0;
        else if (debounce1 == 32'hffffffff) dbtn[0] = 1;
        if (debounce2 == 32'h00000000) dbtn[1] = 0;
        else if (debounce2 == 32'hffffffff) dbtn[1] = 1;
        if (debounce3 == 32'h00000000) dbtn[2] = 0;
        else if (debounce3 == 32'hffffffff) dbtn[2] = 1;
        if (debounce4 == 32'h00000000) dbtn[3] = 0;
        else if (debounce4 == 32'hffffffff) dbtn[3] = 1;
    end
    
    wire clock;
    wire[15:0] scale;
    wire reset;
    wire state;
    
    assign clock = clk;
    assign scale = {sw, 4'h0};
    assign reset = sw[0];
    always @(*) led[3] = state;

    wire[31:0] rx_key;
    wire[31:0] rx_data;
    wire rx_data_valid;
    wire rx_cmd;
    wire rx_act;
    
    always @(*) led[1:0] = rx_key[1:0];
    always @(*) led[2] = rx_cmd;
    
    wire rx_req;
    reg rx_ack = 0;
    always @(clk) rx_ack = rx_req;
    
    reg[31:0] tx_key = 0;
    reg[31:0] tx_data = 0;
    reg tx_data_valid = 0;
    reg tx_cmd = 0;
    wire tx_act;
    
    always @(*) tx_key = {24'h123456, 6'h00, btn[2:1]};
    
    reg tx_req;
    wire tx_ack;
        
    always @(posedge clk) begin
        if (!tx_ack && dbtn[0]) tx_req = 1;
        else if (!dbtn[0]) tx_req = 0;
    end
    
    always @(posedge clk) begin
        
    end
    
    spinlink sl(
        .clock(clock),
        .clock_scale(scale),
        .reset(reset),
        .state(state),
        
        .rx_key(rx_key),
        .rx_data(rx_data),
        .rx_data_valid(rx_data_valid),
        .rx_cmd(rx_cmd),
        .rx_act(rx_act),
        
        .rx_req(rx_req),
        .rx_ack(rx_ack),
        
        .tx_key(tx_key),
        .tx_data(tx_data),
        .tx_data_valid(tx_data_valid),
        .tx_cmd(tx_cmd),
        .tx_act(tx_act),
        
        .tx_req(tx_req),
        .tx_ack(tx_ack),
        
        .rx_tos(rx_tos),
        .rx_tos_ack(rx_tos_ack),
        
        .tx_tos(tx_tos),
        .tx_tos_ack(tx_tos_ack)
    );
    
endmodule
