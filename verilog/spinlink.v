`timescale 1ns / 1ps

module
    spinlink(
        
        // controll signals
        input wire clock,
        input wire[15:0] clock_scale, // internal_clock = clk / (2 * clock_scale) for clock_scale > 0
        input wire reset,
        output reg state = 0,
        
        ///////////////
        // FPGA side //
        ///////////////
        
        // RX
        output wire[31:0] rx_key,
        output wire[31:0] rx_data,
        output wire rx_data_valid,
        output wire rx_cmd,
        output wire rx_act,
        
        output wire rx_req,
        input wire rx_ack,
        
        // TX
        input wire[31:0] tx_key,
        input wire[31:0] tx_data,
        input wire tx_data_valid,
        input wire tx_cmd,
        input wire tx_act,

        input wire tx_req,
        output wire tx_ack,

        ///////////////////
        // SpinLink side //
        ///////////////////
        
        // RX
        input wire[6:0] rx_tos,
        output wire rx_tos_ack,
        
        // TX
        output wire[6:0] tx_tos,
        input wire tx_tos_ack
    );
    
    reg[15:0] counter = 0;
    reg internal_clock = 0;
    
    always @(posedge clock) begin
        if (counter >= clock_scale) begin
            internal_clock = !internal_clock;
            counter = 0;
        end
        counter = counter + 1;
    end
    
    always @(posedge clock) begin
        if (rx_req && !rx_cmd) begin
            if (rx_key == 32'h12345501) state = 1;
            else if (rx_key == 32'h12345502) state = 0;
        end
    end
    
    spinlink_tx tx(
        .clk(internal_clock),
        .rst(reset),
        
        .ack(tx_ack),
        .req(tx_req),
        .key(tx_key),
        .data(tx_data),
        .data_valid(tx_data_valid),
        .cmd(tx_cmd),
        .act(tx_act),
        
        .tos(tx_tos),
        .tos_ack(tx_tos_ack)
    );
    
    spinlink_rx rx(
        .clk(internal_clock),
        .rst(reset),
        
        .tos(rx_tos),
        .tos_ack(rx_tos_ack),
        
        .ack(rx_ack),
        .req(rx_req),
        .key(rx_key),
        .data(rx_data),
        .data_valid(rx_data_valid),
        .cmd(rx_cmd),
        .act(rx_act)
    );
    
endmodule
