`timescale 1ns / 1ps

module
    spinlink_tx(
        // clock & reset
        input wire clk,
        input wire rst,
        
        // high-level interface
        output reg ack = 0,
        input wire req,
        input wire[31:0] key,
        input wire[31:0] data,
        input wire data_valid,
        input wire cmd,
        output reg act = 0,
        
        // low-level interface
        output reg[6:0] tos = 0,
        input wire tos_ack
    );
    
    // conversion table from 4 bit symbols to two-of-seven representation
    function[6:0] nibble2tos;
        input[3:0] nibble;
        begin
                 if (nibble ==  0) nibble2tos = 7'b0010001;
            else if (nibble ==  1) nibble2tos = 7'b0010010;
            else if (nibble ==  2) nibble2tos = 7'b0010100;
            else if (nibble ==  3) nibble2tos = 7'b0011000;
            else if (nibble ==  4) nibble2tos = 7'b0100001;
            else if (nibble ==  5) nibble2tos = 7'b0100010;
            else if (nibble ==  6) nibble2tos = 7'b0100100;
            else if (nibble ==  7) nibble2tos = 7'b0101000;
            else if (nibble ==  8) nibble2tos = 7'b1000001;
            else if (nibble ==  9) nibble2tos = 7'b1000010;
            else if (nibble == 10) nibble2tos = 7'b1000100;
            else if (nibble == 11) nibble2tos = 7'b1001000;
            else if (nibble == 12) nibble2tos = 7'b0010011;
            else if (nibble == 13) nibble2tos = 7'b0010110;
            else if (nibble == 14) nibble2tos = 7'b0011100;
            else if (nibble == 15) nibble2tos = 7'b0001001;
        end
    endfunction
    
    // 72-bit frame data holders
    reg[71:0] frame = 0;
    wire[71:0] temp_frame;
    // constuction the 72 bit package from data, key, cmd, data_valid, and the parity bit of the signals
    assign temp_frame = {data, key, cmd, 5'h00, data_valid, ~^temp_frame[71:1]};
    // state counter
    reg[4:0] nibble_counter = 0;
    // data holder for last tos to obtain the diference from prev time step
    reg last_tos_ack = 0;
        
    always @(posedge clk) begin
        
        // reset behavior
        if (rst) begin
            nibble_counter = 0;
            ack = 0;
            last_tos_ack = 0;
            frame = 0;
            act = 0;
            tos = 0;
        end
        else begin
            if (act) begin
                if (req == 0) ack = 0;
                
                // transmit end-of-packet symbol
                if ((nibble_counter == 10 && !frame[1]) || (nibble_counter == 18 && frame[1])) begin
                    if (last_tos_ack != tos_ack) begin
                        tos = tos ^ 7'b1100000;
                        last_tos_ack = tos_ack;
                        nibble_counter = 0;
                        act = 0;
                    end
                end
                
                // transmit next symbol
                else if (nibble_counter < 18) begin
                    if (last_tos_ack != tos_ack) begin
                        tos = tos ^ nibble2tos(frame[nibble_counter * 4 +: 4]);
                        last_tos_ack = tos_ack;
                        nibble_counter = nibble_counter + 1;
                    end
                end
                
                // end transmission
                else begin
                    nibble_counter = 0;
                    act = 0;
                end
            end
            
            // start transmission
            else begin
                if (req && !ack) begin
                    frame = temp_frame;
                    ack = 1;
                    act = 1;
                    nibble_counter = 0;
                end
                else if (req == 0) ack = 0;
            end
        end
    end
    
endmodule
