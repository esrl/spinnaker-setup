`timescale 1ns / 1ps

module
    spinlink_rx(
        // clock & reset
        input wire clk,
        input wire rst,
        
        // low-level-interface
        input wire[6:0] tos,
        output reg tos_ack = 1,
        
        // high-level-interface
        input wire ack,
        output reg req,
        output reg[31:0] key,
        output reg[31:0] data,
        output reg data_valid,
        output reg cmd,
        output wire act
    );

    // conversion table from two-of-seven representation to 4 bit symbols
    function[4:0] tos2nibble;
        input[6:0] tos;
        begin
                 if (tos == 7'b0010001) tos2nibble = 5'b00000;
            else if (tos == 7'b0010010) tos2nibble = 5'b00001;
            else if (tos == 7'b0010100) tos2nibble = 5'b00010;
            else if (tos == 7'b0011000) tos2nibble = 5'b00011;
            else if (tos == 7'b0100001) tos2nibble = 5'b00100;
            else if (tos == 7'b0100010) tos2nibble = 5'b00101;
            else if (tos == 7'b0100100) tos2nibble = 5'b00110;
            else if (tos == 7'b0101000) tos2nibble = 5'b00111;
            else if (tos == 7'b1000001) tos2nibble = 5'b01000;
            else if (tos == 7'b1000010) tos2nibble = 5'b01001;
            else if (tos == 7'b1000100) tos2nibble = 5'b01010;
            else if (tos == 7'b1001000) tos2nibble = 5'b01011;
            else if (tos == 7'b0000011) tos2nibble = 5'b01100;
            else if (tos == 7'b0000110) tos2nibble = 5'b01101;
            else if (tos == 7'b0001100) tos2nibble = 5'b01110;
            else if (tos == 7'b0001001) tos2nibble = 5'b01111;
            else if (tos == 7'b1100000) tos2nibble = 5'b10000;
            else                        tos2nibble = 5'b11111;
        end
    endfunction
    
    reg[6:0] last_tos = 0;
    reg[6:0] buffer[1:0];
    initial buffer[0] = 0;
    initial buffer[1] = 0;
    wire[6:0] diff = buffer[1] ^ last_tos;
    
    reg[4:0] nibble_counter = 0;
    
    reg[71:0] frame = 0;
    
    assign act = (nibble_counter != 0) ? 1 : 0;
    
    wire[4:0] nibble = tos2nibble(diff);
    
    always @(posedge clk) begin
        if (rst) begin
            tos_ack = !tos_ack;
            req = 0;
            data_valid = 0;
            key = 0;
            data = 0;
            cmd = 0;
            nibble_counter = 0;
            frame = 0;
        end
        else if (req) begin
            if (ack) req = 0;
            frame = 0;
        end
        else begin
            if ((buffer[0] == buffer[1]) && (nibble != 5'b11111)) begin
                if (nibble != 5'b10000) begin
                    frame[nibble_counter * 4 +: 4] = nibble[3:0];
                    nibble_counter = nibble_counter + 1;
                end
                else begin
                    if (^frame) begin
                        if ((nibble_counter == 18) && frame[1]) begin;
                            data_valid = 1;
                            req = 1;
                        end
                        else if ((nibble_counter == 10) && !frame[1]) begin
                            data_valid = 0;
                            req = 1;
                        end
                        key = frame[39:8];
                        data = frame[71:40];
                        cmd = frame[7];
                    end
                    else frame = 0;
                    nibble_counter = 0;
                end
                tos_ack = !tos_ack;
                last_tos = tos;
            end
            buffer[1] = buffer[0];
            buffer[0] = tos;
        end
    end

endmodule
