#!/bin/sh
IMAGE=spinnaker-img
CONTAINER=spinnnaker-con
run()
{
  docker run -it --rm \
    --net=host \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --device="/dev/video0:/dev/video0" \
    --volume="/etc/machine-id:/etc/machine-id" \
    --volume=$(dirname $(readlink -f $0))"/shared:/root/shared/" \
    --name=$CONTAINER \
    $IMAGE\
    bash

  return $?
}

run
