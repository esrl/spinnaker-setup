This container provides low level acces to the spinnaker system using customized tools and bare metel c-code.

The container stores the spinnaker tools at "/root/SupportScripts/spinnaker_tools/".
This folder contains a README for more information of the tools and a directory of test applications.

Further more the container include small tests including ybug scripts and guides on how to run them in /root/shared/low-level/ and in /root/shared/high-level/ .

For more detailed information:

Read the low-level spinnaker README.txt:
$ cat /root/SupportScripts/spinnaker_tools/README.md

Or explore the spinnakers test directory:
$ cd /root/SupportScripts/spinnaker_tools/apps

