# Spinnaker - setup

The repository is meant for easy installation and quick-start of using the spinnaker board and supplied tools as well as local PyNN simulation tools. The repository include a Dockerfile which is used to setup a docker container containing all the tools needed. The tools includes: the Low-level tool-chain along with low level debug tools (ybug); a PyNN simulator for local PyNNsimulation (NEST) and the hihg-level tool-chain for the spinnaker board (sPyNNaker).

Note this repository is only for instalation of the spinnaker/pynn tools. In order to see spinnaker applications see [High-level Spinnaker](https://gitlab.com/esrl/high-level-spinnaker) and [Low-level Spinnaker](https://gitlab.com/esrl/low-level-spinnaker]).

## Installation

The easiest way of getting the tools up and running is using docker. A Docker file is provided which is used to setup all the tools.

#### Docker

If you haven't installed docker, you can find some help here: https://docs.docker.com/cs-engine/1.12/

#### Clone and enter the gitlab repository

    cd
    git clone https://gitlab.com/esrl/spinnaker-setup.git
    cd ~/Spinnaker-setup/

#### Network settings

The spinnaker system can be connected to the host machines in two ways. Either by directly connecting the spinnaker system to the host machine with a ethernet cable, or by connecting both the spinnaker system and the host machine to the same network.

##### Direct connection

If connection is established using direct connection between the spinnaker board and your host machine, you will need to setup some configuration for the ethernet interface on the host machine.

    address 192.168.240.254
    netmask 255.255.255.0
    gateway 0.0.0.0

##### Connection through a router

If connecting through a network, you will need to setup a direct route for the spinnaker system if it does not have the same subnet mask.

    sudo ip addr add 192.168.240.250/24 dev <eth>
    
Where \<eth\> should be the name of your ethernet interface.

##### Build the docker image and run the container

The dockerfile builds a image based on Debian 9 (Jessie).

Before the container can be used, the image needs to be build.

In order to build an image and create a container, run the following command:

    cd ~/Spinnaker-setup/
    docker build -t spinnaker-img .

This will create a image named spinnaker based on the docker file in your current directory.

After building the image, you need to run the container. You can do that with the following command:

    docker run -it --net=host --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" --device="/dev/video0:/dev/video0" --volume="/etc/machine-id:/etc/machine-id" --volume=$(dirname $(readlink -f $0))"/shared:/root/shared/" --rm --name="spinnaker=con" spinnaker bash

You are now inside of the container.

In order to exit the container type:

    exit
    
Remember whenever you are exiting your container everything you have changed inside of it is deleted.

In order to save changes push the changes to git or store the changes in the shared folder: /root/shared/

In order to getting started, explore [High-level Spinnaker](https://gitlab.com/esrl/high-level-spinnaker) and [Low-level Spinnaker](https://gitlab.com/esrl/low-level-spinnaker]).




# Spinnaker FPGA interface

See more on https://gitlab.esrl.dk/SDU-embedded/neuromorphic/Spinnaker_DVS_Integration. (private for now)

The "Spinnaker DVS Integration" repository include hdl files for transmitting and receiving spinlink packages.
